package com.metide.testapp.view

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.metide.testapp.R
import com.metide.testapp.view.adapter.CountryAdapter
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainFragmentTest {

    @get: Rule
    val activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule((MainActivity::class.java))

    @Test
    fun test_visibility_isRecyclerViewVisible() {

        onView(ViewMatchers.withId(R.id.rv_country_list))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_selectItem_isDetailFragmentVisible() {

        onView(ViewMatchers.withId(R.id.rv_country_list))
            .perform(actionOnItemAtPosition<CountryAdapter.CountryViewHolder>(0, click()))

        onView(ViewMatchers.withId(R.id.detail_container))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_backNavigation_toMainFragment() {

        onView(ViewMatchers.withId(R.id.rv_country_list))
            .perform(actionOnItemAtPosition<CountryAdapter.CountryViewHolder>(0, click()))

        onView(ViewMatchers.withId(R.id.detail_container))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        pressBack()

        onView(ViewMatchers.withId(R.id.rv_country_list))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }
}