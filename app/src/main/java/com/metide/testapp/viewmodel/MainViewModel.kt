package com.metide.testapp.viewmodel

import android.app.Application
import android.widget.EditText
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.metide.testapp.NetworkLiveData
import com.metide.testapp.database.getDatabase
import com.metide.testapp.model.Country
import com.metide.testapp.model.Note
import com.metide.testapp.repository.MainRepository
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : ObvAndroidViewModel(application), Observable {

    private val mainRepository = MainRepository(getDatabase(application))
    val countries = mainRepository.countries
    val isLoading = mainRepository.isLoading
    val isConnected = NetworkLiveData

    val selected = MutableLiveData<Country?>()
    var noteOfSelected = MutableLiveData<Note?>()

    @Bindable
    var noteTwoWayText = ""
        set(value) {
            field = value

            // evaluate if Delete Button has to to enabled or not
            canDeleteNote = noteTwoWayText.isNotEmpty() || noteOfSelected.value != null

            // notifying binding of changes
            notifyPropertyChanged(BR.noteTwoWayText)
            notifyPropertyChanged(BR.canDeleteNote)
        }

    @get:Bindable
    var canDeleteNote: Boolean = false

    init {
        refreshDataFromRepository()
    }

    // It downloads data from network
    fun refreshDataFromRepository() {
        mainRepository.refreshCountry()
    }

    // It selects a country as the current, the user maybe want to see its details
    fun selectCountry(item: Country) {
        viewModelScope.launch {
            selected.postValue(item)

            val note = mainRepository.getNoteByCountry(item).firstOrNull()
            noteOfSelected.postValue(note)

            noteTwoWayText = note?.note ?: ""
        }
    }

    // Saves a note for selected country
    fun saveNoteForSelected(editText: EditText) {

        // if user tried to update an existing note with empty text,
        // just delete it from database
        if (noteTwoWayText.isEmpty() && noteOfSelected.value != null) {
            deleteSelectedNote(editText)
            return
        }

        if (noteTwoWayText.isEmpty()) {
            return
        }

        viewModelScope.launch {
            selected.value?.let { country ->
                editText.clearFocus()

                mainRepository.saveNoteForCountry(
                    country,
                    noteTwoWayText,
                    noteOfSelected.value
                )

                val note = mainRepository.getNoteByCountry(country).firstOrNull()
                noteOfSelected.postValue(note)
            }
        }
    }

    // It deletes the note for selected country from local DB and resets the view
    fun deleteSelectedNote(editText: EditText) {
        viewModelScope.launch {

            noteOfSelected.value?.let { note ->
                mainRepository.deleteNote(note)
                noteOfSelected.value = null
            }

            editText.clearFocus()
            noteTwoWayText = ""
        }
    }

    fun isNoteChanged(): Boolean {
        return (noteOfSelected.value != null && noteTwoWayText != noteOfSelected.value?.note)
                || (noteOfSelected.value == null && noteTwoWayText.isNotEmpty())
    }
}