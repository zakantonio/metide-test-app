package com.metide.testapp.view.base

import android.app.Application
import com.metide.testapp.NetworkLiveData

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        NetworkLiveData.init(this)
    }
}