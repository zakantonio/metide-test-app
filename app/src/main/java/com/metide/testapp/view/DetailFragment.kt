package com.metide.testapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.metide.testapp.R
import com.metide.testapp.databinding.DetailFragmentBinding
import com.metide.testapp.view.base.BaseFragment
import com.metide.testapp.viewmodel.MainViewModel

class DetailFragment : BaseFragment() {

    companion object {
        fun newInstance() = DetailFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding: DetailFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.detail_fragment, container, false
        )

        ViewModelProvider(requireActivity()).get(MainViewModel::class.java).apply {
            viewModel = this
            binding.mainViewModel = this
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        enableBackButton(true)
    }

    override fun onBackPressed(): Boolean {
        if (viewModel.isNoteChanged()) {

            AlertDialog.Builder(requireContext()).apply {
                setTitle(getString(R.string.alert_title))
                setMessage(getString(R.string.alert_unsaved_msg))
                setPositiveButton(
                    getString(R.string.alert_action_ok),
                    null
                )
            }.show()

            return true
        }
        return super.onBackPressed()
    }
}