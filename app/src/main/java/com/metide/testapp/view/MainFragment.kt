package com.metide.testapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.metide.testapp.R
import com.metide.testapp.databinding.MainFragmentBinding
import com.metide.testapp.view.adapter.CountryAdapter
import com.metide.testapp.view.base.BaseActivity
import com.metide.testapp.view.base.BaseFragment
import com.metide.testapp.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : BaseFragment() {

    companion object {
        fun newInstance() = MainFragment()

        val TAG = MainFragment::class.simpleName
    }

    private lateinit var viewModel: MainViewModel

    // When user clicks on item, app goes to the DetailFragment
    private var adapter = CountryAdapter(onItemClick = { item ->

        if (activity is BaseActivity) {
            val baseActivity = activity as BaseActivity

            lifecycleScope.launchWhenResumed {

                // Save the selected country item
                viewModel.selectCountry(item)

                baseActivity.getFrameId()?.let { frameId ->
                    baseActivity.replaceFragment(DetailFragment.newInstance(), frameId)

                } ?: Log.e(
                    TAG,
                    "WARNING: activity doesn't override getFrameId() correctly from BaseActivity."
                )
            }
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding: MainFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment, container, false
        )

        ViewModelProvider(requireActivity()).get(MainViewModel::class.java).apply {
            viewModel = this
            binding.mainViewModel = this
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        enableBackButton(false)

        rv_country_list.adapter = adapter

        swipe_refresh.setOnRefreshListener {
            viewModel.refreshDataFromRepository()
        }

        viewModel.isLoading.observe(viewLifecycleOwner, {
            swipe_refresh.isRefreshing = it
        })

        viewModel.countries.observe(viewLifecycleOwner, {
            adapter.setData(it)
        })
    }
}