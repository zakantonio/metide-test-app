package com.metide.testapp.view

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.metide.testapp.NetworkLiveData
import com.metide.testapp.R
import com.metide.testapp.view.base.BaseActivity
import com.metide.testapp.viewmodel.MainViewModel

class MainActivity : BaseActivity() {

    private lateinit var viewModel: MainViewModel

    override fun getFrameId() = R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.isConnected.observe(this, { isConnected ->
            if (isConnected) {
                // Toast.makeText(activity, "Connected", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(
                    this,
                    getString(R.string.generic_connection_lost),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}