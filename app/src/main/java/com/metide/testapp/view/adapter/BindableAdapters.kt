package com.metide.testapp.view.adapter

interface BindableAdapters<T> {
    fun setData(data: T?)
}