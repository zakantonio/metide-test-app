package com.metide.testapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.metide.testapp.databinding.ItemCountryBinding
import com.metide.testapp.model.Country
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class CountryAdapter(val onItemClick: (item: Country) -> Unit) :
    RecyclerView.Adapter<CountryAdapter.CountryViewHolder>(),
    BindableAdapters<List<Country>> {

    private var list: List<Country> = listOf()

    override fun setData(data: List<Country>?) {
        CoroutineScope(Dispatchers.Main).launch {

            // Sort countries list based on the distance
            // between each country and Metide Headquarter,
            // from nearest, to farthest
            list = data?.sortedBy { current ->

                current.distance

            } ?: listOf()

            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemCountryBinding.inflate(layoutInflater, parent, false)
        return CountryViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(list[position])
        holder.itemView.setOnClickListener {
            onItemClick(list[position])
        }
    }

    override fun getItemCount(): Int = list.count()

    class CountryViewHolder(private val binding: ItemCountryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(country: Country) {
            binding.country = country
            binding.executePendingBindings()
        }
    }
}