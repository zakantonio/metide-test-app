package com.metide.testapp.view.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.metide.testapp.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class BaseActivity : AppCompatActivity() {

    companion object {
        val TAG = BaseActivity::class.simpleName
    }

    abstract fun getFrameId(): Int?

    /**
     * Notify every fragment on back pressed
     * to handle it, if needed.
     */
    override fun onBackPressed() {

        var handled = false
        for (f in supportFragmentManager.fragments) {
            if (f is BaseFragment) {
                handled = f.onBackPressed()
                if (handled) {
                    break
                }
            }
        }

        if (!handled) {
            super.onBackPressed()
        }
    }

    fun replaceFragment(
        f: BaseFragment,
        frameId: Int,
        fragmentTag: String? = f::class.simpleName,
        addBckStack: Boolean = true,
        allowStateLoss: Boolean = false
    ) {

        val existingFragment = supportFragmentManager.findFragmentByTag(fragmentTag)
        if (null != existingFragment && existingFragment.isAdded && existingFragment.isDetached) {
            return
        }

        if (isFinishing) {
            return
        }

        lifecycleScope.launchWhenResumed {
            withContext(Dispatchers.Main) {
                val fragmentTransaction = supportFragmentManager.beginTransaction()

                fragmentTransaction.setCustomAnimations(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )

                fragmentTransaction.replace(frameId, f, fragmentTag)

                if (addBckStack) {
                    fragmentTransaction.addToBackStack(fragmentTag)
                }

                if (allowStateLoss) {
                    fragmentTransaction.commitAllowingStateLoss()
                } else {
                    fragmentTransaction.commit()
                }
            }
        }
    }
}