package com.metide.testapp.view.base

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    /**
     * Handle back press in fragment.
     * @return true if back press was handled
     */
    open fun onBackPressed(): Boolean {
        return false
    }

    fun enableBackButton(enable: Boolean) {
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(enable)
    }
}