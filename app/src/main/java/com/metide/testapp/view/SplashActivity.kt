package com.metide.testapp.view

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.metide.testapp.view.base.BaseActivity
import kotlinx.coroutines.delay

class SplashActivity : BaseActivity() {

    override fun getFrameId(): Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launchWhenCreated {
            delay(3000L)

            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }
    }
}