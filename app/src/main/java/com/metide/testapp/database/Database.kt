package com.metide.testapp.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CountryDao {

    @Query("SELECT * FROM countries_table")
    fun getCountries(): LiveData<List<DBCountry>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(countries: List<DBCountry>)

    @Transaction
    @Query("SELECT * FROM countries_table WHERE id = :id")
    fun getCountryAndNote(id: String): LiveData<List<CountryAndNote?>>

    @Query("SELECT * FROM note_table")
    fun getNotes(): LiveData<List<DBCountryNote>>

    @Query("SELECT * FROM note_table WHERE id = :id")
    suspend fun getNoteById(id: Int): DBCountryNote

    @Query("SELECT * FROM note_table WHERE countryId = :countryId")
    suspend fun getNoteByCountryId(countryId: Int): List<DBCountryNote>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(note: DBCountryNote)

    @Query ("DELETE FROM note_table WHERE id = :noteId")
    suspend fun deleteNote(noteId: Int)
}

@Database(entities = [DBCountry::class, DBCountryNote::class], version = 2)
abstract class CountriesDatabase : RoomDatabase() {
    abstract val countryDao: CountryDao
}

private lateinit var INSTANCE: CountriesDatabase

fun getDatabase(context: Context): CountriesDatabase {
    synchronized(CountriesDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                CountriesDatabase::class.java,
                "countries"
            ).build()
        }
    }
    return INSTANCE
}