package com.metide.testapp.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.metide.testapp.model.Country

@Entity(tableName = "countries_table")
data class DBCountry constructor(

    @PrimaryKey
    val id: Int,
    val enabled: Boolean,
    val code3l: String,
    val code2l: String,
    val name: String,
    val name_official: String,
    val flag: String,
    val latitude: Double,
    val longitude: Double,
    val zoom: String,
    val distance: Float
)

fun List<DBCountry>.asDomainModel() = map { it.asDomainModel() }

fun DBCountry.asDomainModel(): Country {
    return Country(
        id = this.id,
        enabled = this.enabled,
        code3l = this.code3l,
        code2l = this.code2l,
        name = this.name,
        name_official = this.name_official,
        flag = this.flag,
        latitude = this.latitude,
        longitude = this.longitude,
        zoom = this.zoom,
        distance = this.distance
    )
}