package com.metide.testapp.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.metide.testapp.model.Note

@Entity(tableName = "note_table")
data class DBCountryNote constructor(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val note: String,
    val countryId: Int
)

fun List<DBCountryNote>.asDomainModel() = map { it.asDomainModel() }

fun DBCountryNote.asDomainModel(): Note {
    return Note(
        id = this.id,
        note = this.note,
        countryId = this.countryId
    )
}