package com.metide.testapp.database

import androidx.room.Embedded
import androidx.room.Relation

data class CountryAndNote(

 @Embedded
 val dbCountry: DBCountry,

 @Relation(
  parentColumn = "id",
  entityColumn = "countryId"
 )
 val dbCountryNote: DBCountryNote
)
