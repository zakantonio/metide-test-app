package com.metide.testapp.network

import com.metide.testapp.BuildConfig
import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.io.IOException


interface NetworkApiInterface {

    @GET("countries")
    fun getCountries(): Call<List<NetworkCountry>>
}

object NetworkClient {

    private lateinit var retrofit: NetworkApiInterface

    private var client = OkHttpClient.Builder()
        .addInterceptor(BasicAuthInterceptor(BuildConfig.networkUsername, BuildConfig.networkPwd))
        .build()


    fun getNetworkClient(): NetworkApiInterface {
        if (!::retrofit.isInitialized) {
            val adapter: Retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.networkBaseUrl)
                .client(client)
                .build()

            retrofit = adapter.create(
                NetworkApiInterface::class.java
            )
        }

        return retrofit
    }
}

class BasicAuthInterceptor(user: String, password: String) : Interceptor {

    private val credentials: String = Credentials.basic(user, password)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest: Request = request.newBuilder()
            .header("Authorization", credentials).build()
        return chain.proceed(authenticatedRequest)
    }
}