package com.metide.testapp.network

data class NetworkCountry(
    val id: Int,
    val enabled: Int,
    val code3l: String,
    val code2l: String,
    val name: String,
    val name_official: String,
    val flag: String,
    val latitude: Double,
    val longitude: Double,
    val zoom: String
)