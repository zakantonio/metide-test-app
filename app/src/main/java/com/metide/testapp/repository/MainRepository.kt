package com.metide.testapp.repository

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.metide.testapp.Constants
import com.metide.testapp.database.CountriesDatabase
import com.metide.testapp.database.DBCountry
import com.metide.testapp.database.DBCountryNote
import com.metide.testapp.database.asDomainModel
import com.metide.testapp.model.Country
import com.metide.testapp.model.Note
import com.metide.testapp.network.NetworkClient
import com.metide.testapp.network.NetworkCountry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainRepository(private val database: CountriesDatabase) {

    val isLoading = MutableLiveData(false)

    val countries: LiveData<List<Country>> =
        Transformations.map(database.countryDao.getCountries()) {
            it.asDomainModel()
        } as MutableLiveData<List<Country>>

    /**
     * [saveNoteForCountry] You can insert a new note or update it if already exist for the passed country
     */
    suspend fun saveNoteForCountry(country: Country, note: String, existingNote: Note? = null) {

        database.countryDao.insertNote(
            DBCountryNote(
                id = existingNote?.id ?: 0,
                note = note,
                countryId = country.id
            )
        )
    }

    suspend fun deleteNote(note: Note) {
        database.countryDao.deleteNote(note.id)
    }

    suspend fun getNoteByCountry(country: Country): List<Note> {
        return database.countryDao.getNoteByCountryId(country.id).asDomainModel()
    }

    fun refreshCountry() {

        isLoading.postValue(true)

        NetworkClient.getNetworkClient().getCountries()
            .enqueue(object : Callback<List<NetworkCountry>> {
                override fun onResponse(
                    call: Call<List<NetworkCountry>>,
                    response: Response<List<NetworkCountry>>
                ) {
                    isLoading.postValue(false)

                    if (response.isSuccessful) {
                        CoroutineScope(Dispatchers.Main).launch {

                            database.countryDao.insertAll(
                                response.body()?.asDatabaseModel() ?: listOf()
                            )
                        }
                    }
                }

                override fun onFailure(call: Call<List<NetworkCountry>>, t: Throwable) {
                    isLoading.postValue(false)
                }
            })
    }
}

fun List<NetworkCountry>.asDatabaseModel() = map { it.asDatabaseModel() }

fun NetworkCountry.asDatabaseModel(): DBCountry {

    val locationA = Location("")

    locationA.latitude = Constants.HQ_LAT
    locationA.longitude = Constants.HQ_LGT

    val locationB = Location("")

    locationB.latitude = this.latitude
    locationB.longitude = this.longitude

    val distance = locationA.distanceTo(locationB)

    return DBCountry(
        id = this.id,
        enabled = this.enabled == 1,
        code3l = this.code3l,
        code2l = this.code2l,
        name = this.name,
        name_official = this.name_official,
        flag = this.flag,
        latitude = this.latitude,
        longitude = this.longitude,
        zoom = this.zoom,
        distance = distance
    )
}