package com.metide.testapp.model

data class Country(
    val id: Int,
    val enabled: Boolean,
    val code3l: String,
    val code2l: String,
    val name: String,
    val name_official: String,
    val flag: String,
    val latitude: Double,
    val longitude: Double,
    val zoom: String,
    val distance: Float
) {

    fun getDistanceFormatted() : String  {

        return "${"%.1f".format(distance / 1000)} km"
    }
}