package com.metide.testapp.model

data class Note(
    val id: Int,
    val note: String,
    val countryId: Int
)