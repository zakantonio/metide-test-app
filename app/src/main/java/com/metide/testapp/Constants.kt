package com.metide.testapp

object Constants {

    // Metide headquarter geo location taken from Google Maps
    const val HQ_LAT = 45.5542836
    const val HQ_LGT = 12.3013954
}