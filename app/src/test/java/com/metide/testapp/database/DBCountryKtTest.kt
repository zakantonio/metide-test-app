package com.metide.testapp.database

import org.junit.Test

class DBCountryKtTest {

    @Test
    fun asDomainModel_mapping() {

        val list = arrayListOf<DBCountry>()

        for (i in 0..9) {
            list.add(
                DBCountry(
                    id = i, enabled = true,
                    code3l = "code3l - $i",
                    code2l = "code2l - $i",
                    name = "name - $i",
                    name_official = "name_official - $i",
                    flag = "",
                    latitude = 0.0,
                    longitude = 0.0,
                    zoom = ""
                )
            )
        }

        val result = list.asDomainModel()

        assert(result.count() == list.count())
    }
}